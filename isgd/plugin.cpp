#include "plugin.h"

const char* const SHRINK_URL = "http://is.gd/create.php";
const char* const UNSHRINK_URL = "";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("isgd");
}

QString Plugin::site() const
{
    return QString("http://is.gd");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return false;
}

void Plugin::shrink(const QString &url)
{
    QUrl apiurl(SHRINK_URL);

#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("format", "json");
    query.addQueryItem("url", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("format", "json");
    apiurl.addQueryItem("url", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
    manager->get(request)->ignoreSslErrors();
}

void Plugin::unShrink(const QString &url)
{
    Q_UNUSED(url);
}

#ifdef QT5
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("shorturl").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("shorturl").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif

void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}

#ifndef QT5
Q_EXPORT_PLUGIN2( isgd, Plugin )
#endif // QT_VERSION < 0x050000
