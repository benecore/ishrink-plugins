#ifndef PLUGIN_H
#define PLUGIN_H

#include <QtPlugin>
#include <QtNetwork>
#include "../plugininterface.h"

#ifdef QT5
#include <QJsonDocument>
#include <QUrlQuery>
#else
#include "../json.h"
#endif

#ifdef QT_DEBUG
#include <QDebug>
#endif

class Plugin : public PluginInterface
{
    Q_OBJECT
#ifdef QT5
    Q_PLUGIN_METADATA(IID "com.devpda.ishrink.interface" FILE "plugin.json")
#endif // QT_VERSION >= 0x050000
    Q_INTERFACES(PluginInterface)
public:
    Plugin(QObject *parent = 0);
    ~Plugin();

public slots:
    QString name() const;
    QString site() const;
    QString version() const;
    QString author() const;
    bool unShrinkSupport() const;
    void shrink(const QString &url);
    void unShrink(const QString &url);

private slots:
    void replyShrink(QNetworkReply *reply);
    void replyUnShrink(QNetworkReply *reply);

private:
    QNetworkAccessManager *manager;
};

#endif // PLUGIN_H
