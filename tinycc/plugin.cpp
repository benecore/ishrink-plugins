#include "plugin.h"

const char* const SHRINK_URL = "http://tiny.cc/";
const char* const UNSHRINK_URL = "http://tiny.cc/";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("tinycc");
}

QString Plugin::site() const
{
    return QString("http://tiny.cc");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return true;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("c", "rest_api");
    query.addQueryItem("login", "iShrinkApp");
    query.addQueryItem("apiKey", "d9fa7500-200a-4768-87d5-70a3d1259487");
    query.addQueryItem("version", "2.0.3");
    query.addQueryItem("m", "shorten");
    query.addQueryItem("format", "json");
    query.addQueryItem("longUrl", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("c", "rest_api");
    apiurl.addQueryItem("login", "iShrinkApp");
    apiurl.addQueryItem("apiKey", "d9fa7500-200a-4768-87d5-70a3d1259487");
    apiurl.addQueryItem("version", "2.0.3");
    apiurl.addQueryItem("m", "shorten");
    apiurl.addQueryItem("format", "json");
    apiurl.addQueryItem("longUrl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
#ifdef QT5
    manager->get(request);
#else
    manager->get(request);
#endif
}

void Plugin::unShrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(UNSHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("c", "rest_api");
    query.addQueryItem("login", "iShrinkApp");
    query.addQueryItem("apiKey", "d9fa7500-200a-4768-87d5-70a3d1259487");
    query.addQueryItem("version", "2.0.3");
    query.addQueryItem("m", "expand");
    query.addQueryItem("format", "json");
    query.addQueryItem("shortUrl", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("c", "rest_api");
    apiurl.addQueryItem("login", "iShrinkApp");
    apiurl.addQueryItem("apiKey", "d9fa7500-200a-4768-87d5-70a3d1259487");
    apiurl.addQueryItem("version", "2.0.3");
    apiurl.addQueryItem("m", "expand");
    apiurl.addQueryItem("format", "json");
    apiurl.addQueryItem("shortUrl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyUnShrink(QNetworkReply*)));
#ifdef QT5
    manager->get(request);
#else
    manager->get(request);
#endif
}


#ifdef QT5
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("results").toObject().value("short_url").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("results").toMap().value("short_url").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif



#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT UNSHRINK" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("results").toObject().value("longUrl").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("results").toMap().value("longUrl").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif


#ifndef QT5
Q_EXPORT_PLUGIN2( tinycc, Plugin )
#endif // QT_VERSION < 0x050000
