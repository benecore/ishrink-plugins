#include "plugin.h"

const char* const SHRINK_URL = "https://www.googleapis.com/urlshortener/v1/url";
const char* const UNSHRINK_URL = "https://www.googleapis.com/urlshortener/v1/url";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("google");
}

QString Plugin::site() const
{
    return QString("http://goo.gl");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return true;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("key", "AIzaSyBDarmyWjbmSwK9ZbyNwQ09jfJxsaQeewo");
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("key", "AIzaSyBDarmyWjbmSwK9ZbyNwQ09jfJxsaQeewo");
#endif

    QByteArray postParams;
    postParams += "{\"longUrl\":";
    postParams += "\""+url+"\"}";

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
    manager->post(request, postParams)->ignoreSslErrors();
}

void Plugin::unShrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(UNSHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("key", "AIzaSyBDarmyWjbmSwK9ZbyNwQ09jfJxsaQeewo");
    query.addQueryItem("shortUrl", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("key", "AIzaSyBDarmyWjbmSwK9ZbyNwQ09jfJxsaQeewo");
    apiurl.addQueryItem("shortUrl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyUnShrink(QNetworkReply*)));
    manager->get(request)->ignoreSslErrors();
}

#ifdef QT5
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("id").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("id").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif

#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT UNSHRINK" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("longUrl").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("longUrl").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif


#ifndef QT5
Q_EXPORT_PLUGIN2( google, Plugin )
#endif // QT_VERSION < 0x050000
