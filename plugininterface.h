#ifndef PLUGININTERFACE
#define PLUGININTERFACE

#include <QObject>
#include <QString>

class PluginInterface : public QObject
{
    Q_OBJECT
public:
    explicit PluginInterface(QObject *parent = 0) : QObject(parent) {}

    virtual ~PluginInterface() {}

    /**
     * @brief Plugin name | example: MyGooglePlugin
     * @return plugin name
     */
    virtual QString name() const = 0;
    /**
     * @brief Plugin site | example: http://goo.gl
     * @return plugin site
     */
    virtual QString site() const = 0;
    /**
     * @brief Plugin version (major.minor.patch) | example: 0.0.1
     * @return plugin version
     */
    virtual QString version() const = 0;
    /**
     * @brief Author of the plugin (Pinocchio :D)
     * @return author
     */
    virtual QString author() const = 0;
    /**
     * @brief unShrinkSupport support unshrink
     * @return
     */
    virtual bool unShrinkSupport() const = 0;
    /**
     * @brief Shrink url
     * @param url
     */
    virtual void shrink(const QString &url) = 0;
    /**
     * @brief unShrink
     * @param url
     */
    virtual void unShrink(const QString &url) = 0;


signals:
    /**
     * @brief Emitted when request passed
     * @param response from server
     */
    void done(const QByteArray response);
    /**
     * @brief Emitted when an error occurred
     * @param errorCode
     * @param errorString
     */
    void error(const int errorCode, const QString errorString);
};

Q_DECLARE_INTERFACE( PluginInterface, "com.devpda.ishrink.interface" )

#endif // PLUGININTERFACE

