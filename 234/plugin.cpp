#include "plugin.h"

const char* const SHRINK_URL = "http://234.sk/create.php";
const char* const UNSHRINK_URL = "";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("234");
}

QString Plugin::site() const
{
    return QString("http://234.sk");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return true;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("url", url);
    //apiurl.setQuery(query);
#else
    QUrl query;
    query.addQueryItem("url", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
#ifdef QT5
    manager->post(request, query.query().toUtf8());
#else
    manager->post(request, query.encodedQuery());
#endif
}

void Plugin::unShrink(const QString &url)
{
    Q_UNUSED(url)
}


void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        int fieldsetStartIndex = result.indexOf("class=\"sturl_field\" value=");
        int fieldsetEndIndex = result.indexOf("/>\n</div>");
        QString prin = result.mid(fieldsetStartIndex, (fieldsetEndIndex-fieldsetStartIndex));
        int startIndex = prin.indexOf("value=")+6;
        int endIndex = prin.indexOf("' '");
        QByteArray shortUrl = prin.mid(startIndex, endIndex-startIndex).toUtf8().replace("\"", "").trimmed();
        emit done(shortUrl);
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}


#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#endif


#ifndef QT5
Q_EXPORT_PLUGIN2( 234, Plugin )
#endif // QT_VERSION < 0x050000
