#include "plugin.h"

const char* const SHRINK_URL = "http://url.googluj.cz/index.php";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("googluj");
}

QString Plugin::site() const
{
    return QString("http://googluj.cz");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return false;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("longurl", url);
#else
    QUrl query;
    query.addQueryItem("longurl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
#ifdef QT5
    manager->post(request, query.toString().toUtf8());
#else
    manager->post(request, query.encodedQuery());
#endif
}

void Plugin::unShrink(const QString &url)
{
    Q_UNUSED(url);
}


void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();

        int fieldsetStartIndex = result.indexOf("<fieldset>");
        int fieldsetEndIndex = result.indexOf("</fieldset>");
        QString prin = result.mid(fieldsetStartIndex, (fieldsetEndIndex-fieldsetStartIndex));
        int startIndex = prin.indexOf("href")+6;
        int endIndex = prin.indexOf("\">");
        QByteArray shortUrl = prin.mid(startIndex, endIndex-startIndex).toUtf8();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        emit done(shortUrl);
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}


void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}


#ifndef QT5
Q_EXPORT_PLUGIN2( googluj, Plugin )
#endif // QT_VERSION < 0x050000
