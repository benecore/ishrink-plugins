#include "plugin.h"

const char* const SHRINK_URL = "http://B54.in/api/";
const char* const UNSHRINK_URL = "";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("b54in");
}

QString Plugin::site() const
{
    return QString("http://b54.in");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return false;
}

void Plugin::shrink(const QString &url)
{
    QUrl apiurl(SHRINK_URL);

#if QT_VERSION >= 0x050000
    QUrlQuery query;
    query.addQueryItem("action", "shorturl");
    query.addQueryItem("url", url);
    query.addQueryItem("format", "json");
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("action", "shorturl");
    apiurl.addQueryItem("url", url);
    apiurl.addQueryItem("format", "json");
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
#ifdef QT5
    manager->post(request, apiurl.query().toUtf8())->ignoreSslErrors();
#else
    manager->post(request, apiurl.encodedQuery())->ignoreSslErrors();
#endif
}

void Plugin::unShrink(const QString &url)
{
    Q_UNUSED(url);
}

#ifdef QT5
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("shorturl").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("shorturl").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif

#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#endif

#ifndef QT5
Q_EXPORT_PLUGIN2( b54in, Plugin )
#endif // QT_VERSION < 0x050000
