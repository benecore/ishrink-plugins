#include "plugin.h"

const char* const SHRINK_URL = "http://www.chod.sk/get";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("chod");
}

QString Plugin::site() const
{
    return QString("http://chod.sk");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return false;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("url", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("url", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
    manager->get(request);
}

void Plugin::unShrink(const QString &url)
{
    Q_UNUSED(url);
}


void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        emit done(result);
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}

#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    Q_UNUSED(reply)
}
#endif


#ifndef QT5
Q_EXPORT_PLUGIN2( chod, Plugin )
#endif // QT_VERSION < 0x050000
