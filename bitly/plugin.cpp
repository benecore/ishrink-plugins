#include "plugin.h"

const char* const SHRINK_URL = "https://api-ssl.bitly.com/v3/shorten";
const char* const UNSHRINK_URL = "https://api-ssl.bitly.com/v3/expand";

Plugin::Plugin(QObject *parent) :
    PluginInterface(parent)
{
    manager = new QNetworkAccessManager(this);
}

Plugin::~Plugin()
{
    delete manager;
    manager = 0;
}

QString Plugin::name() const
{
    return QString("bitly");
}

QString Plugin::site() const
{
    return QString("http://bitly.com");
}

QString Plugin::version() const
{
    return QString("1.0");
}

QString Plugin::author() const
{
    return QString("benecore");
}

bool Plugin::unShrinkSupport() const
{
    return true;
}

void Plugin::shrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(SHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("access_token", "23786440aae330fa4ed438a261f564779ca785b9");
    query.addQueryItem("longUrl", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("access_token", "23786440aae330fa4ed438a261f564779ca785b9");
    apiurl.addQueryItem("longUrl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyShrink(QNetworkReply*)));
#ifdef QT5
    manager->get(request);
#else
    manager->get(request);
#endif
}

void Plugin::unShrink(const QString &url)
{
    disconnect(manager, 0, 0, 0);
    QUrl apiurl(UNSHRINK_URL);
#ifdef QT5
    QUrlQuery query;
    query.addQueryItem("access_token", "23786440aae330fa4ed438a261f564779ca785b9");
    query.addQueryItem("shortUrl", url);
    apiurl.setQuery(query);
#else
    apiurl.addQueryItem("access_token", "23786440aae330fa4ed438a261f564779ca785b9");
    apiurl.addQueryItem("shortUrl", url);
#endif

    QNetworkRequest request(apiurl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(replyUnShrink(QNetworkReply*)));
#ifdef QT5
    manager->get(request);
#else
    manager->get(request);
#endif
}


#ifdef QT5
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("data").toObject().value("url").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("data").toMap().value("url").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif



#ifdef QT5
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT UNSHRINK" << result << endl;
#endif
        QJsonParseError jsonError;
        QJsonDocument doc = QJsonDocument::fromJson(result, &jsonError);
        if (jsonError.error == QJsonParseError::NoError){
            QJsonObject object = doc.object();
            emit done(object.value("data").toObject().value("expand").toArray().at(0).toObject().value("long_url").toString().toUtf8());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#else
void Plugin::replyUnShrink(QNetworkReply *reply)
{
    if (!reply->error()){

        QByteArray result = reply->readAll();
#ifdef QT_DEBUG
        qDebug() << "RESULT" << result << endl;
#endif
        bool ok;
        QVariant json = QtJson::Json::parse(result, ok);
        if (ok){
            emit done(json.toMap().value("data").toMap().value("expand").toList().at(0).toMap().value("long_url").toByteArray());
        }else{
            emit error(1, QByteArray());
        }
    }else{
        emit error(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    delete reply;
}
#endif


#ifndef QT5
Q_EXPORT_PLUGIN2( bitly, Plugin )
#endif // QT_VERSION < 0x050000
