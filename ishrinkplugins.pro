#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T15:39:34
#
#-------------------------------------------------
TEMPLATE = subdirs
SUBDIRS += \
        b54in \
        bitly \
        chod \
        jdem \
        googluj \
        tinycc \
        tinyurl \
        isgd \
        vgd \
        toly \
        urlie \
        google \
        rddme \
        twgs \
        dopice \
        a5 \
        ix \
        234
HEADERS += plugininterface.h

greaterThan(QT_MAJOR_VERSION, 4){
    message(toto je 5 verzia)
    SOURCES -= json.cpp
    HEADERS -= json.h
}else{
    message(a toto zase nie je)
    SOURCES += json.cpp
    HEADERS += json.h
}
