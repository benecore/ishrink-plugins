#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T15:39:34
#
#-------------------------------------------------

QT       += core gui network
TARGET = vgd
TEMPLATE = lib
CONFIG += plugin
DESTDIR = ../lib

SOURCES += plugin.cpp

HEADERS += plugin.h \
    ../plugininterface.h
DISTFILES += plugin.json

greaterThan(QT_MAJOR_VERSION, 4){
    DEFINES += QT5
    SOURCES -= ../json.cpp
    HEADERS -= ../json.h
}else{
    DEFINES -= QT5
    SOURCES += ../json.cpp
    HEADERS += ../json.h
}

unix {
    target.path = /usr/lib
    INSTALLS += target
}
